<?php

use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'My Yii Application';
?>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],

        'name',
        [
            'label' => 'books_title',
            'value' => function ($model) {
                $cellData = [];

                foreach($model->getBook()->all() as $item){
                    $cellData[] = $item->title;
                }

                return implode(', ', $cellData);
            }
        ],

    ],
]); ?>
