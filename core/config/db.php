<?php

return [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=yii2_db:3306;dbname=yii',
    'username' => 'yii',
    'password' => 'yii',
    'charset' => 'utf8',

    // Schema cache options (for production environment)
    //'enableSchemaCache' => true,
    //'schemaCacheDuration' => 60,
    //'schemaCache' => 'cache',
];
