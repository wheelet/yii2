<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%numbers}}`.
 */
class m190822_195428_create_numbers_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $this->createTable('{{%numbers}}', [
            'id' => $this->string()->unique(),
            'number' => $this->integer(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
        $this->dropTable('{{%numbers}}');
    }
}
