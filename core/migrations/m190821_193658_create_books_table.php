<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%books}}`.
 */
class m190821_193658_create_books_table extends Migration
{
    /**
     * @return bool|void
     */
    public function up()
    {
        $this->createTable('books', [
            'id' => $this->primaryKey()->unique(),
            'title' => $this->string()->notNull(),
            'author_id' => $this->smallinteger(8)->notNull(),
        ]);


        $this->alterColumn('books', 'id', $this->smallInteger(8).' NOT NULL AUTO_INCREMENT');

        // creates index for column `author_id`
        $this->createIndex(
            'idx-books-author_id',
            'books',
            'author_id'
        );

        // add foreign key for table `authors`
        $this->addForeignKey(
            'fk-books-author_id',
            'books',
            'author_id',
            'authors',
            'id',
            'CASCADE'
        );
    }

    /**
     * @return bool|void
     */
    public function down()
    {
        $this->dropForeignKey(
            'fk-books-author_id',
            'books'
        );

        // drops index for column `author_id`
        $this->dropIndex(
            'idx-books-author_id',
            'books'
        );

        $this->dropTable('books');
    }
}
