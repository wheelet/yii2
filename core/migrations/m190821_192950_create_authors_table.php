<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%authors}}`.
 */
class m190821_192950_create_authors_table extends Migration
{
    /**
     * @return bool|void
     */
    public function up()
    {
        $this->createTable('authors', [
            'id' => $this->primaryKey()->unique(),
            'name' => $this->string()->notNull(),
            'surname' => $this->string()->notNull(),
        ]);

        $this->alterColumn('authors', 'id', $this->smallInteger(8).' NOT NULL AUTO_INCREMENT');
    }

    /**
     * @return bool|void
     */
    public function down()
    {
        $this->dropTable('authors');
    }
}
