<?php

namespace app\controllers;

use app\models\Number;
use yii\rest\ActiveController;
use yii\web\Response;

class NumberController extends ActiveController
{
    public $modelClass = 'app\models\Number';

    public function actionGenerate()
    {
        $newnumber = new Number();
        $newnumber->id = spl_object_hash($newnumber);
        $newnumber->number = random_int(1,100);
        $newnumber->save();

        return $newnumber->id;
    }

    public function actionRetrieve($id)
    {
        $num = Number::find()->where(['id' => $id])->one();

        return $num->number;
    }

}
